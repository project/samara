

module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      css: {
        files: [
          'scss/*.scss',
          'scss/*/*.scss'
        ],
        tasks: ['sass']
      },
      templates: {
        files: ['templates/*.twig'],
        tasks: ['exec']
      },
      livereload: {
        files: ['css/**.css'],
        options: {
          livereload: true
        }
      }
    },

    exec: {
      cache_rebuild: 'drush cr'
    },

    sass: {
      options: {
        outputStyle: 'expanded',
        sourceMap: true
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'scss',
          src: ['*.scss', '*/*.scss'],
          dest: 'css',
          ext: '.css'
        }]

      }
    }

  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-exec');

  grunt.registerTask('default', ['watch']);
};
