<?php

/**
 * @file
 * Theme settings form.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function samara_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['samara'] = [
    '#type' => 'details',
    '#title' => t('Samara'),
    '#open' => TRUE,
  ];

  $form['samara']['container_width'] = [
    '#type' => 'number',
    '#title' => t('Container width'),
    '#min' => 800,
    '#max' => 1600,
    '#step' => 80,
    '#default_value' => theme_get_setting('container_width'),
  ];

  $form['samara']['font_size'] = [
    '#type' => 'number',
    '#title' => t('Font size'),
    '#min' => 12,
    '#max' => 18,
    '#default_value' => theme_get_setting('font_size'),
  ];

  $fonts['Standard'] = [
    'impact' => 'Impact',
    'palatino_linotype' => 'Palatino Linotype',
    'tahoma' => 'Tahoma',
    'century-gothic' => 'Century Gothic',
    'lucida-sans-unicode' => 'lucida Sans Unicode',
    'arial-black' => 'Arial Black',
    'times-new-roman' => 'Times New Roman',
    'arial-warrow' => 'Arial Narrow',
    'verdana' => 'Verdana',
    'copperplate-gothic_light' => 'Copperplate Gothic Light',
    'lucida-console' => 'Lucida Console',
    'gill-sans' => 'Gill Sans',
    'trebuchet-ms' => 'Trebuchet MS',
    'courier-new' => 'Courier New',
    'arial' => 'Arial',
    'georgia' => 'Georgia',
  ];

  $fonts['Google fonts'] = [
    'ubuntu' => 'Ubuntu ',
    'open-sans' => 'Open Sans',
    'josefin-slab' => 'Josefin Slab',
    'pt-sans' => 'PT Sans',
    'roboto' => 'Roboto',
    'source-sans-pro' => 'Sanc Pro',
    'exo' => 'Exo',
  ];

  $form['samara']['font_family'] = [
    '#type' => 'select',
    '#title' => t('Font family'),
    '#options' => $fonts,
    '#default_value' => theme_get_setting('font_family'),
  ];

}
