/**
 * @file
 * Small UI improvements.
 */
(function ($) {

  Drupal.behaviors.samara = {
    attach: function(context) {

      $('.messages')
        .prepend('<span class="hide-message">×</span>')
        .not('.view-changed').fadeIn(1000);

      $('.hide-message')
        .click(function() {
          $(this).parent().fadeOut()
        });

    }
  };

  var $body = $('body');

  var $menuWrapper = $('.menu--main').find('.block-content');
  $menuWrapper
    .prepend('<div id="hamburger">☰</div>');

  var $menu = $menuWrapper.find('> .menu');
  var $hamburger = $('#hamburger');
  $hamburger.click(function() {
    if ($body.hasClass('js-menu-expanded')) {
      $body.removeClass('js-menu-expanded');
      $body.addClass('js-menu-collapsed');
      $hamburger.text('☰');
    }
    else {
      $body.removeClass('js-menu-collapsed');
      $body.addClass('js-menu-expanded');
      $hamburger.text('✕');
    }
  });

  // Remove empty title wrapper.
  // @see: https://www.drupal.org/node/953034
  var $pageTitle = $('.block-page-title-block');
  if (!$pageTitle.text().trim()) {
    $pageTitle.remove();
  }

})(jQuery, Drupal);
