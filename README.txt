SAMARA
==============
A flexible Drupal theme with a responsive layout.

CONFIGURATION
==============
Enabling dropdown menu for main navigation:
1. Navigate to admin/structure/menu/manage/main page and create multilevel
   hierarchy for the menu.
2. For each menu item check 'Show as expanded' checkbox.
3. Navigate to admin/structure/block page and make sure that 'Main navigation'
   block is placed to 'Primary menu' region.

REGIONS
==============
Samara supports the following regions:
+-----------------------------------------+
| +-------------------------------------+ |
| |            Primary menu             | |
| +-------------------------------------+ |
| +-------------------------------------+ |
| |              Header                 | |
| +-------------------------------------+ |
| +-------------------------------------+ |
| |            Breadcrumbs              | |
| +-------------------------------------+ |
| +---------+ +---------------+---------+ |
| | Sidebar | |    Content    | Sidebar | |
| | first   | |               | second  | |
| |         | |               |         | |
| |         | |               |         | |
| |         | |               |         | |
| |         | |               |         | |
| +---------+ +---------------+---------+ |
| +-------------------------------------+ |
| |  Footer       Footer        Footer  | |
| |  first        second        third   | |
| +-------------------------------------+ |
+-----------------------------------------+

SUPPORT
==============
Use the issue queue in drupal.org for questions or bug reports
https://www.drupal.org/project/issues/samara
